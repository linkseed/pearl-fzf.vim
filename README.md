# fzf.vim for Pearl

Things you can do with fzf and Vim.

## Details

- Plugin: https://github.com/junegunn/fzf.vim
- Pearl: https://github.com/pearl-core/pearl
